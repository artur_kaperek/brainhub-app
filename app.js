angular.module('currencyConverter', [])
    .controller('MainCtrl',['$scope', function ($scope) {
        var that = this;

        $scope.currencyPlnConversations = [];

        $scope.addNewTile = function (originalCurrencyAmount) {
            var randomEuroExchange = 4.2 * (1 + (getRandomNumber() - 50)/1000),
                newPlnPrice = randomEuroExchange * originalCurrencyAmount;

            function getRandomNumber() {
                return  parseInt(Math.random() * 100);
            }

            $scope.currencyPlnConversations.push({
                price: newPlnPrice.toFixed(5),
                time: moment().format('YYYY.MM.DD HH:mm')
            });
        };

        console.log("Hello 312");
    }]);