var gulp = require('gulp'),
    sass = require('gulp-sass'),
    webserver = require('gulp-webserver'),
    styleSource = 'style/*.scss';

gulp.task('sass', function() {
    gulp.src(styleSource)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./style'));
});

gulp.task('sass:watch', function () {
    gulp.watch(styleSource, ['sass']);
});

gulp.task('webserver', function() {
    gulp.src('')
        .pipe(webserver({
            livereload: true,
            directoryListing: true,
            open: true,
            port: 3000
        }));
});

gulp.task('default', ['sass', 'sass:watch', 'webserver']);